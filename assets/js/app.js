var todoItems = [];
// --- render list todo -----
function renderTodo(todo) {
    var table = $('.list-todo table')
    var trTodo = document.createElement("tr");
    trTodo.setAttribute('key', todo.id);
    trTodo.innerHTML = `
        <td><input type="checkbox" onclick="Complete(${todo.id})"></td>
        <td class="content-todo">${todo.text}</td>
        <td><img onclick="Delete(${todo.id})" src="./assets/img/delete.png" alt="delete"></td>
    `;
    table.append(trTodo);
}

// -------- render list complete------
function renderDone(todo) {
    var table = $('.list-done .done');
    var tr = document.createElement("tr");
    tr.setAttribute('key', todo.id);
    tr.innerHTML = `
        <td class="content-todo">${todo.text}</td>
        <td><img onclick="Delete(${todo.id})" class="del" src="./assets/img/delete.png" alt="delete"></td>
    `;
    table.append(tr);
}

// ------- Function add list todo----------
function addTodo(text) {
    const todo = {
      text,
      isDone: false,
      id: Date.now(),
    };
    todoItems.push(todo);
    renderTodo(todo);
}

// --- Add to do Item ----
function addTodoItem(){
    event.preventDefault();
    var input = $('.add-form input');
    let newtodo = input.val().trim();
    if (newtodo !== '') {
        addTodo(newtodo);
        input.val("");
        input.focus();
    }
    DisplayReset(todoItems.length);
    TitleTodo(CountItem(todoItems, todoItems.length));
}

var form = $(".add-form");
form.on('submit', function(){
    addTodoItem();
})

// ----- Set Complete -------

function Complete(id){
    var item = $(`.list-todo table tr[key='${id}']`)
    for(i=0; i<todoItems.length; i++){
        if(todoItems[i].id == id){
            todoItems[i].isDone = true;
            renderDone(todoItems[i])
        }
    }
    item.remove();
    TitleTodo(CountItem(todoItems, todoItems.length));
}

// ------------- Delete -----------
function Delete(id){
    var item = $(`tr[key='${id}']`)
    item.remove();
    for(i=0; i<todoItems.length; i++){
        if(todoItems[i].id == id){
            todoItems.splice(i,1);
        }
    }
    if(todoItems.length == 0){
        DisplayReset(todoItems.length);
    }
    TitleTodo(CountItem(todoItems, todoItems.length));
}

// ------------ Delete All ----------
function DeleteAll(){
    var items = $('tr');
    items.remove();
    todoItems.splice(0, todoItems.length);
    DisplayReset(todoItems.length);
    console.log(todoItems)
    TitleTodo(CountItem(todoItems, todoItems.length));

}

// -------------------------------
function DisplayReset(num){
    if(num !=0){
        $(".reset").addClass("block");
    }else{
        $(".reset").removeClass("block");
    }
}

function TitleTodo(arr){
    var todo = $('.list-todo h3')
    todo[0].innerHTML=`List Todo (${arr[0]})`;

    var done = $('.list-done h3')
    done[0].innerHTML=`List Complete (${arr[1]})`;
}

function CountItem(arr, l){
    var count = [0, 0];
    for(i=0; i<l; i++){
        if(arr[i].isDone){
            count[1]++;
        }else{
            count[0]++;
        }
    }
    return count;
}
